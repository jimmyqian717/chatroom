<!DOCTYPE html>
<?php
	session_start();
	$uname = $_SESSION['uname'];
	//include("sender.php");
?>
<html>
<head>
	<title>Chat room</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" type="text/css" href="css/chatroom.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body onload = "load()">

	<div id="textArea">
		<h3 class = "w3-center">Welcome, <?php echo $uname; ?></h3>
		<input id = "currentU" type = "text" style = "opacity:0;user-select:none;z-index:999;cursor:default" value ="<?php echo $uname; ?>" >
		<p id = "messages"></p>
	</div> 
	<input type="text" name="newMsg" id = "msgBar" placeholder="Say something..">
	<button type="button" id = "sendBtn" >Send</button>
	
</body>
</html>
<script>
	$("#sendBtn").click(function(){
		send();
	});
	$("#msgBar").keypress (function(e){
		var code = e.keyCode || e.which;
		if (code == 13){
			send();
		}	
		
	});
	function send(){
		var $uname = $("#currentU").val();
		var $message = $("#msgBar").val();
		$.ajax({
			type: "POST",
			url: 'sender.php',
			data: {newMsg:$message,cu:$uname},
			success: function(data) {
				$("#msgBar").val("");
			}
		});
	}
</script>
<script>	 
	function load() {
	    var myVar = setInterval(update, 500);
	}
	
	function update() {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("messages").innerHTML = this.responseText;

			}
		};
		xhttp.open("POST", "messages.txt", true);
		xhttp.send();

	}

</script>
